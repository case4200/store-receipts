import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import java.time.LocalDate;
import java.time.format.*;


public class project {
	// ATTENTION!!!
	// IMPORTANT: Make sure that you Replace 'eleal' by your
	// Postgres Username in the following string
	//
	// You need to have an SSH tunnel from localhost 63333 to akka:5432 for
	// this code to work.
	String jdbcUrl = "jdbc:postgresql://localhost:63333/case0237";
	// For security reasons, never PUT YOUR PASSWORD
	// IN YOUR SOURCE FILE LIKE THIS:
	// String password = "This is my password. Now, steal my money!";
	Connection conn;
	/**
	 * This class gets the current DB connection. This is not to be used in
	 * production environments. You should use a connection pool instead.
	 *
	 * @return
	 * @throws SQLException
	 */
	public Connection getDBConnection() throws SQLException {
		if (conn == null) {
			// Display a message to get the password from the user
			JLabel label = new JLabel("Postgres Username: ");
			JTextField jtf = new JTextField();
			JLabel label2 = new JLabel("Postgres Password:");
			JPasswordField jpf = new JPasswordField();
			JOptionPane.showConfirmDialog(null,
					new Object[]{label, jtf, label2, jpf},
					"Password:", JOptionPane.OK_CANCEL_OPTION);
			String password = String.valueOf(jpf.getPassword());
			conn = DriverManager.getConnection(jdbcUrl, jtf.getText(), password);
		}
		conn.setAutoCommit(true);
		return conn;
	}

	public static void main(String[] args) throws Exception{
		project DBMSp1 = new project();
		Scanner s = new Scanner(System.in);

		System.out.println("Query 1 or 2?");
		final int user_pick = s.nextInt();

		if(user_pick == 1) {

			System.out.println("Enter the receipt id");
			final int receipt_id = s.nextInt();
			System.out.println("Enter the piano id");
			final int serial_no = s.nextInt();
			System.out.println("Enter the customer id");
			final int client_id = s.nextInt();
			s.nextLine();
			System.out.println("Enter the name of the salesperson");
			final String salesperson_name = s.nextLine(); // read in the newline character left by the previous call to nextInt()
			System.out.println("Enter the amount paid");
			final BigDecimal amount_paid = s.nextBigDecimal();
		


			DBMSp1.query1(receipt_id, serial_no, client_id, salesperson_name, amount_paid);
		}
		else
			if (user_pick == 2) {
																															
              
				//Enter the make and model for the piano
			    System.out.println("Enter the make");
			    final String make = s.next(); 
			    System.out.println("Enter the model");
			    final String model = s.next();
			    
			
			   DBMSp1.query2(make, model);

			}
			else {
				System.out.println("Error: user can only choose 1 or 2");
			}
	
	}
	public boolean query1(int receipt_id, int serial_no, int client_id, String salesperson_name, BigDecimal amount_paid) throws SQLException {
	    System.out.println("starting");
	    assert salesperson_name != null;
	    // Connect to the database.
	    getDBConnection();
	    System.out.println("connected");
	    int updatedRows = 0;

	    // Get the current date
	    LocalDate currentDate = LocalDate.now();
	    java.sql.Date date_purchased = java.sql.Date.valueOf(currentDate);


	    String sqlInsertReceipt =
	            "INSERT INTO receipt (receipt_id, serial_no, client_id, salesperson_name, date_purchased, amount_paid) " +
	            "VALUES(?,?,?,?,?,?)";

	    try (PreparedStatement receiptInsertStmt = conn.prepareStatement(sqlInsertReceipt);) {
	        // Disable auto-commit mode
	        conn.setAutoCommit(false);

	        receiptInsertStmt.setInt(1, receipt_id);
	        receiptInsertStmt.setInt(2, serial_no);
	        receiptInsertStmt.setInt(3, client_id);
	        receiptInsertStmt.setString(4, salesperson_name);
	        receiptInsertStmt.setDate(5, date_purchased); // Set the current date as date_purchased
			receiptInsertStmt.setBigDecimal(6, amount_paid); // Set the amount_paid value from piano table

	        // Run the insert receipt query
	        updatedRows = receiptInsertStmt.executeUpdate();
	        
	        System.out.println("Receipt Details:");
	        System.out.println("Receipt ID: " + receipt_id);
	        System.out.println("Serial No: " + serial_no);
	        System.out.println("Client ID: " + client_id);
	        System.out.println("Salesperson Name: " + salesperson_name);
	        System.out.println("Date Purchased: " + date_purchased);
	        System.out.println("Amount Paid: " + " $" + amount_paid);
	        

	        // Commit the transaction
	        conn.commit();
	    } catch (SQLException e) {
	        // Rollback the transaction
	        conn.rollback();
	        e.printStackTrace();
	    } finally {
	        // Enable auto-commit mode
	        conn.setAutoCommit(true);
	    }
	    return updatedRows != 0;
	}
	public boolean query2(String make, String model) throws SQLException {
		System.out.println("starting");
	    assert make != null;
	    assert model != null;
	  
	    // Connect to the database.
	    getDBConnection();
	    System.out.println("connected");
	    int updatedRows = 0;
	    // Elaborate a string with the content of the query to select a piano
	    String sqlSelect = "SELECT * FROM piano WHERE make = ? AND model = ?";
	    

	    // Use a try-with-resources block to ensure the PreparedStatement is closed
	    try (PreparedStatement selectStmt = conn.prepareStatement(sqlSelect);) {
	        selectStmt.setString(1, make);
	        selectStmt.setString(2, model);

	        // If the SELECT statement returns a result set, there are matching rows
	        //if (hasResults) {
	        ResultSet r = selectStmt.executeQuery();
	        while (r.next()) {
	            // Loop through each row of the result set and print the values
	            int id = r.getInt("serial_no");
	            String makeResult = r.getString("make");
	            String modelResult = r.getString("model");
	            Date yearResult = r.getDate("year");
	            String priceResult = r.getString("MSRP");
                
	            System.out.println(id + " " + makeResult + " " + modelResult + " " + yearResult + " " + priceResult);
	         
	            updatedRows++;
	        }
	        //System.out.print("Done");
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return updatedRows != 0;
	}
}